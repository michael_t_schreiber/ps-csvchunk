$inputFile = $args[0]
$BatchSizeNumLines = [int]$args[1]

$inputFileName = $inputFile | split-path -Leaf

if ( !(test-path $inputFile) -or $inputFile -notlike "*.csv")
{
    throw "Error: Argument is not a csv file"
}
$scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path
$invokeDir = (get-location).Path
$inputFileNameBase = $inputFileName.Substring(0,$inputFileName.Length-4)

$HeaderLine = get-content $inputFile -TotalCount 1 -Encoding UTF8
$DataLines = [System.IO.File]::ReadAllLines($inputFile) | Select-Object -Skip 1
$DataLineCount = $DataLines.Count
$DataLineRemainder = $DataLines.Count % $BatchSizeNumLines
$DataLineThousands = ( ( $DataLineCount - $DataLineRemainder ) / $BatchSizeNumLines )

for ($i = 1; $i -le $DataLineThousands ; $i++)
{
    $exportLines = $DataLines[(($i-1)*$BatchSizeNumLines)..($i*$BatchSizeNumLines-1)]
    $exportFilenameAppendDigit = $i
    $HeaderLine | Out-File -FilePath "$invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv" -Append -Encoding utf8
    $exportLines | Out-File -FilePath "$invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv" -Append -Encoding utf8
    Write-Host "Wrote Batch $exportFilenameAppendDigit to $invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv"
}

if ($DataLineRemainder -gt 0)
{
    $ExportLines = $DataLines | Select-Object -last $DataLineRemainder
    $exportFilenameAppendDigit =  $DataLineThousands + 1
    $HeaderLine | Out-File -FilePath "$invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv" -Append -Encoding utf8
    $exportLines | Out-File -FilePath "$invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv" -Append -Encoding utf8
    Write-Host "Wrote Batch $exportFilenameAppendDigit to $invokeDir\$($inputFileNameBase)_CHUNK$exportFilenameAppendDigit.csv"
}